import { Injectable, NotFoundException } from '@nestjs/common';
import { LessThanOrEqual, MoreThanOrEqual, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Cat } from './cat.entity';

@Injectable()
export class CatsService {
  constructor(@InjectRepository(Cat) private repo: Repository<Cat>) {}

  create(name: string, age: number, breed: string) {
    try {
      const cat = this.repo.create({ name, age, breed });
      return this.repo.save(cat);
    } catch (err) {
      throw err;
    }
  }
  async getCats() {
    try {
      const result = await this.repo.find();
      return result;
    } catch (err) {
      throw err;
    }
  }
  async getCatById(id: number) {
    try {
      const cat = await this.repo.findOne(id);
      if (!cat) {
        throw new NotFoundException('Cat Not Found');
      }
      return cat;
    } catch (err) {
      throw err;
    }
  }
  async editCat(id: number, updatedData: Partial<Cat>) {
    try {
      const cat = await this.getCatById(id);
      if (!cat) {
        throw new NotFoundException('Cat Not Found');
      }
      Object.assign(cat, updatedData);
      return this.repo.save(cat);
    } catch (err) {
      throw err;
    }
  }

  async deleteCat(id: number) {
    try {
      const cat = await this.getCatById(id);
      if (!cat) {
        throw new NotFoundException('Cat Not Found');
      }
      return this.repo.remove(cat);
    } catch (err) {
      throw err;
    }
  }

  async searchCat(leftAge: number, rightAge: number) {
    try {
      const cats = await this.repo.find({
        where: {
          age: LessThanOrEqual(rightAge) && MoreThanOrEqual(leftAge),
        },
      });
      if (!cats) {
        throw new NotFoundException('Cat Not Found');
      }
      return cats;
    } catch (err) {
      throw err;
    }
  }
}
