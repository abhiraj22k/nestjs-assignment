import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { CreateCatDto } from './dtos/create-cat.dto';
import { CatsService } from './cats.service';
import { UpdateCatDto } from './dtos/update-cat.dto';

@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}
  @Post('/create')
  createCat(@Body() body: CreateCatDto) {
    this.catsService.create(body.name, body.age, body.breed);
  }
  @Get('/list')
  getCats() {
    const result = this.catsService.getCats();
    return result;
  }
  @Put('edit/:id')
  editCat(@Param('id') id: string, @Body() body: UpdateCatDto) {
    const result = this.catsService.editCat(parseInt(id), body);
    return result;
  }
  @Delete('delete/:id')
  deleteCat(@Param('id') id: string) {
    const result = this.catsService.deleteCat(parseInt(id));
    return result;
  }
  @Get('/search')
  searchCat(
    @Query('age_lte') age_lte: string,
    @Query('age_rte') age_rte: string,
  ): Promise<any> {
    const result = this.catsService.searchCat(
      parseInt(age_lte),
      parseInt(age_rte),
    );
    return result;
  }
}
