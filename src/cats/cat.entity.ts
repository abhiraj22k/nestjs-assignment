import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
} from 'typeorm';

@Entity()
export class Cat {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  age: number;
  @Column()
  breed: string;

  @AfterInsert()
  logInsert() {
    console.log('Inserted Cat with id', this.id);
  }
  @AfterUpdate()
  logUpdate() {
    console.log('Updated Cat with id', this.id);
  }
}
